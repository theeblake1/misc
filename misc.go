package misc

import (
	"encoding/base64"
	"net/mail"
	"strconv"
	"strings"
	"gopkg.in/guregu/null.v3"
)

// StringInSlice checks if a string exists in a slice
func StringInSlice(val string, list []string) bool {
	for _, key := range list {
		if key == val {
			return true
		}
	}
	return false
}

// IntInSlice checks if an int64 exists in a slice
func IntInSlice(val int64, list []int64) bool {
	for _, key := range list {
		if key == val {
			return true
		}
	}
	return false
}

// UniqueIntSlice takes a slice and removes duplicates
func UniqueIntSlice(intSlice []int64) []int64 {
	keys := make(map[int64]bool)
	list := []int64{}
	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

// UniqueStringSlice takes a slice and removes duplicates
func UniqueStringSlice(intSlice []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

// IntSliceEqual checks if two int slices are equal
func IntSliceEqual(a, b []int64) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

// StringSliceEqual checks if two string slices are equal
func StringSliceEqual(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

// NewTrue returns a boolean pointer to a value of true
func NewTrue() *bool {
	b := true
	return &b
}

// NewFalse returns a boolean pointer to a value of false
func NewFalse() *bool {
	b := false
	return &b
}

// NullInt returns a reference to a null.Int
func NullInt(v interface{}) *null.Int {
	const op errors.Op = "misc.nullInt"

	nullInt := &null.Int{}
	switch v.(type) {
	case int:
		{
			nullInt.SetValid(int64(v.(int)))
		}
	case *int:
		{
			val := v.(*int)
			nullInt.SetValid(int64(*val))
		}
	case int32:
		{
			nullInt.SetValid(int64(v.(int32)))
		}
	case *int32:
		{
			nullInt.SetValid(int64(*v.(*int32)))
		}
	case int64:
		{
			nullInt.SetValid(v.(int64))
		}
	case *int64:
		{
			val := v.(*int64)
			nullInt.SetValid(*val)
		}
	case string:
		{
			val, _ := strconv.Atoi(v.(string))
			nullInt.SetValid(int64(val))
		}
	case *string:
		{
			val, _ := strconv.Atoi(*v.(*string))
			nullInt.SetValid(int64(val))
		}
	default:
		{
			return nullInt
		}
	}
	return nullInt
}

// NullFloat returns a reference to a null.Float
func NullFloat(v interface{}) *null.Float {
	const op errors.Op = "misc.nullFloat"

	nullFloat := &null.Float{}
	switch v.(type) {
	case int:
		{
			nullFloat.SetValid(float64(v.(int)))
		}
	case *int:
		{
			nullFloat.SetValid(float64(*v.(*int)))
		}
	case int32:
		{
			nullFloat.SetValid(float64(v.(int32)))
		}
	case *int32:
		{
			nullFloat.SetValid(float64(*v.(*int32)))
		}
	case int64:
		{
			nullFloat.SetValid(float64(v.(int64)))
		}
	case *int64:
		{
			nullFloat.SetValid(float64(*v.(*int64)))
		}
	case float32:
		{
			nullFloat.SetValid(float64(v.(float32)))
		}
	case *float32:
		{
			nullFloat.SetValid(float64(*v.(*float32)))
		}
	case float64:
		{
			nullFloat.SetValid(v.(float64))
		}
	case *float64:
		{
			nullFloat.SetValid(*v.(*float64))
		}
	case string:
		{
			val, _ := strconv.Atoi(v.(string))
			nullFloat.SetValid(float64(val))
		}
	case *string:
		{
			val, _ := strconv.Atoi(*v.(*string))
			nullFloat.SetValid(float64(val))
		}
	default:
		{
			return nullFloat
		}
	}
	return nullFloat
}

// NullString returns a reference to a null.String
func NullString(v interface{}) *null.String {
	const op errors.Op = "misc.nullString"

	nullString := &null.String{}
	switch v.(type) {
	case int:
		{
			val := strconv.Itoa(v.(int))
			nullString.SetValid(val)
		}
	case *int:
		{
			val := strconv.Itoa(*v.(*int))
			nullString.SetValid(val)
		}
	case int32:
		{
			val := strconv.Itoa(int(v.(int32)))
			nullString.SetValid(val)
		}
	case *int32:
		{
			val := strconv.Itoa(int(*v.(*int32)))
			nullString.SetValid(val)
		}
	case int64:
		{
			val := strconv.Itoa(int(v.(int64)))
			nullString.SetValid(val)
		}
	case *int64:
		{
			val := strconv.Itoa(int(*v.(*int64)))
			nullString.SetValid(val)
		}
	case float32:
		{
			val := strconv.Itoa(int(v.(float32)))
			nullString.SetValid(val)
		}
	case *float32:
		{
			val := strconv.Itoa(int(*v.(*float32)))
			nullString.SetValid(val)
		}
	case float64:
		{
			val := strconv.Itoa(int(v.(float64)))
			nullString.SetValid(val)
		}
	case *float64:
		{
			val := strconv.Itoa(int(*v.(*float64)))
			nullString.SetValid(val)
		}
	case string:
		{
			nullString.SetValid(v.(string))
		}
	case *string:
		{
			nullString.SetValid(*v.(*string))
		}
	default:
		{
			return nullString
		}
	}
	return nullString
}

// EncodeRFC2047 encodes our email subject
func EncodeRFC2047(s string) string {
	addr := mail.Address{Name: s, Address: ""}
	return strings.Trim(addr.String(), " <>\"@")
}

// EncodeWeb64String encodes our email body
func EncodeWeb64String(b []byte) string {
	s := base64.URLEncoding.EncodeToString(b)
	var i = len(s) - 1
	for s[i] == '=' {
		i--
	}
	return s[0 : i+1]
}