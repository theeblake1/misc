package errors

import (
	"fmt"
)

// Op is a unique string describing a method or a function
// Multiple operations can construct a friendly stack tace.
type Op string

// Kind is a string categorizing the error
type Kind string

// TODO: define kind constants
// const (
// 	// ObjectNotFound is a response kind to communicate back to our client that the object is not found
// 	ObjectNotFound = http.StatusNotFound
// 	// OK is a response kind to communicate back to our client that the request w
// 	OK           = http.StatusOK
// 	Created      = http.StatusCreated
// 	BadRequest   = http.StatusBadRequest
// 	Unauthorized = http.StatusUnauthorized
// 	NotAllowed   = http.StatusMethodNotAllowed
// )

// Error is a custom Error struct for quickly diagnosing issues with our app
type Error struct {
	Op     Op     `json:"operation,omitempty"` // operation
	Kind   Kind   `json:"kind,omitempty"`      // category of errors
	Err    error  `json:"err,omitempty"`       // the wrapped error
	ErrMsg string `json:"errMsg,omitempty"`
	Msg    string `json:"customMsg,omitempty"`
	//... application specific fields
}

func (e *Error) Error() string {
	if e.Msg != "" {
		return fmt.Sprintf("%v: %v\n%v", e.Op, e.Err, e.Msg)
	}

	return fmt.Sprintf("%v: %v", e.Op, e.Err)
}

// E is a helper method for filling our Error Struct
func E(args ...interface{}) error {
	e := &Error{}
	for _, arg := range args {
		switch arg := arg.(type) {
		case Op:
			e.Op = arg
		case error:
			e.Err = arg
		case Kind:
			e.Kind = arg
		case string:
			e.Msg = arg
		default:
			panic("bad call to E")
		}
	}
	if e.Err != nil {
		e.ErrMsg = e.Err.Error()
	}
	return e
}

// Ops is a recursive function for building a slice of operations
func Ops(e *Error) []Op {
	res := []Op{e.Op}

	subErr, ok := e.Err.(*Error)
	if !ok {
		return res
	}

	res = append(res, Ops(subErr)...)

	return res
}

// Msgs is a recursive function for building a slice of Messages
func Msgs(e *Error) []string {
	res := []string{e.Msg}

	subErr, ok := e.Err.(*Error)
	if !ok {
		return res
	}

	res = append(res, Msgs(subErr)...)

	return res
}

// LastMsg retrieves the first message on an error chain
func LastMsg(e *Error) string {
	msgs := Msgs(e)
	return msgs[len(msgs)-1]
}
